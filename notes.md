Some commands that are worth investigating and possibly including in our
(more pythonic) abstraction/library:

mountimage, cloneimage, createliveclone, deletimage, expireimage, exportimage,
failover, backup, lsbackup, lsdevice, lsjobhistory, lspolicy, lsuser, lsuserrole,
lsvm, lsrestoreoptions, lsworkflow, lsappclass, mkhost, refreshliveclone, restoreimage
unmountimage, verifyimage, vmdiscovery

Different commands I ran at different times to grab info I needed for the mountimage
command. The Python API is rudimentary, and not pythonic at all. Lots of room for
improvement there.

```
mnt = appliance.simple_mount(
	source_application=app, target_host=host, mount_image=None, 
	restoretime='', strict_policy=False, pre_script='', post_script='', 
	nowait=True, job_class='snapshot', mount_mode='nfs', label='Python Library', **kwargs)
	
mnt = appliance.simple_mount(source_application=app, target_host=host, mount_image=image)
```

*List Workflow options for an object_id or a application*

```
mnt = appliance.run_uds_command('info', 'lsworkflow', {'filtervalue': 'object_id=389855'})
mnt = appliance.run_uds_command('info', 'lsworkflow', {'filtervalue': 'yls1kyle'})
```

*Get all SARG reports:* `pprint(get_reports_all(appliance))`

List the appclass options for Oracle

```
mnt = appliance.run_uds_command('info', 'lsappclass', {'name': 'Oracle'})
pprint(mnt)
```

*List Backups for an image:*

```
mnt = appliance.run_uds_command('info', 'lsbackup', {'filtervalue': 'backupname={}'.format(image)})
pprint(mnt)

appliance.run_uds_command('info','lsbackup', {'filtervalue': 'appname=yls1', '': 'backupdate since 24 hours'})

```

*restoreoptions for target host:*

```
mnt = appliance.run_uds_command('info', 'lsrestoreoptions', {'applicationtype': 'Oracle', 'action': 'mount', 'targethost': host.id})
pprint(mnt)
```