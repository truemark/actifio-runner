#!/usr/bin/env python3
"""
Utility to work with Actifio.

Performs operations on Actifio appliances using Actifio's REST API.
Currently (January 2020) it can only perform a few operations, such as listing images,
reports, job status, and perform some image operations, such as unmounting/deleting
of mounted images, and mounting images to a target database.
"""

import sys, os.path
from getpass import getpass
import logging
import argparse
import functools
import time
import urllib
import yaml
import ipaddress
import shlex
from pprint import pformat, pprint
import Actifio
from datetime import datetime
from collections.abc import MutableMapping

__author__ = "Roberto Mello"
__copyright__ = "TrueMark, LLC"
__credits__ = ["Roberto Mello"]
__version__ = "0.1.1"
__maintainer__ = "Roberto Mello"
__email__ = "roberto.mello@gmail.com"
__status__ = "Production"

class TruemarkActifio:
    def __init__(self, host: str, username: str, password: str, verbose=False):
        self.host = host
        self.username = username
        self.appliance = Actifio.Actifio(host, username, password, verbose=verbose)
        self.current_ts = datetime.now()
        self.config = None
        logging.info("At {} connected to appliance {}".format(self.current_ts, self.appliance))

        # FIXME: Change these to be dynamically discovered. Not currently used anyways.
        self.source_dbs = {
            'ylvoprd_stbypdx4': 1234382973,
            'ylprd_stbypdx41': 3438348,
            'yls1_stbypdx4': 3438246,
            'ylprd': 266876,
            'ylvoprd': 266786,
            'yls1': 10510283,
            'jdeprd': 14295354
        }
    # end

    def load_config(self, config):
        # TODO: do checks, etc
        self.config = config
    # end

    @staticmethod
    def recursive_merge(dict1, dict2):
        '''
        Update two dicts of dicts recursively, if either mapping has leaves that
        are non-dicts, the second's leaf overwrites the first's.
        Ex: merged = recursive_merge(first_dict, second_dict)

        Modified from https://stackoverflow.com/a/24088493
        '''
        def merge(d1, d2):
            for k, v in d1.items():  # in Python 2, use .iteritems()!
                if k in d2:
                    if all(isinstance(e, MutableMapping) for e in (v, d2[k])):
                        d2[k] = TruemarkActifio.recursive_merge(v, d2[k])
                    # we could further check types and merge as appropriate here.
            d3 = d1.copy()
            d3.update(d2)
            return d3
        # end
        return functools.reduce(merge, (dict1, dict2))
    # end

    def to_datetime(self, ts: str):
        """
        Given a timestamp string ts in format %Y-%m-%d %H:%M:%S.%f, return the
        datetime object for it.

        :param ts: timestamp string in format %Y-%m-%d %H:%M:%S.%f
        :return: datetime object for the timestamp
        """
        # FIXME: add checks, exceptions, etc
        return datetime.strptime(ts, '%Y-%m-%d %H:%M:%S.%f')
    # end

    def get_quoted_recovery_time(self, recovery_time):
        """
        Returns a url-quoted timestamp string of today's date plus the recovery_time
        passed in.
        :param recovery_time: HH:MM:SS to be used for Point-In-Time-Recovery (PITR)
        :return: string
        """
        return urllib.parse.quote('{}-{:02d}-{} {}'.format(
            self.current_ts.year, self.current_ts.month, self.current_ts.day, recovery_time))
    # end

    def get_db_params(self, source_db, target_db, ora_version):
        """
        Returns a dictionary with app_id, post_script, ora_home, tns_admin_home that
        can be used in actifio oracle image operations

        :param source_db: source database name
        :param target_db: target database name
        :param ora_version: oracle version
        :return: dict()
        """
        # let's keep the url-unquoted strings here for better readability
        ora_home = urllib.parse.quote('u01/app/oracle/product/{}/dbhome_1'.format(ora_version))

        if source_db in self.source_dbs.keys():
            app_id = self.source_dbs[source_db]
            post_script = 'script=' + urllib.parse.quote(
                'phase=POST:name=actifio-post-kyle.sh:args={},{},{}'.format(
                    source_db, target_db, ora_version))
        else:
            # get images and app_ids
            post_script = None

        return {
            'app_id': app_id,
            'post_script': post_script,
            'ora_home': ora_home,
            'tns_admin_home': '{}/network/admin'.format(ora_home)
        }
    # end

    def find_latest_image(self, images):
        """
        Given an ActImageCollection (a collection of ActImage objects), return the
        image object that is the latest, based on the image's backupdate attribute.

        :param images: ActImageColletion
        :return: ActImage object with the newest backupdate
        """
        latest = None

        for image in images:
            ts = self.to_datetime(image.objectdata['backupdate'])

            if latest is None:
                latest = {'image': image, 'ts': ts}
            elif ts > latest['ts']:
                latest['image'] = image
                latest['ts'] = ts
        # end for

        return latest['image']
    # end

    def run_report(self, report_name, report_options=""):
        """
        Returns a dictionary containing reportname -> description of SARG (Simple
        Actifio Reports Generator) reports. No idea how to get the options for each
        report, as there's no documentation I can find.

        :return: dictionary
        """
        result = dict()
        options = dict()
        tokens = shlex.split(report_options)
        token_index = 0

        while token_index < len(tokens):
            next_token = token_index + 1
            
            if tokens[token_index].startswith('-'):
                if next_token < len(tokens) and not tokens[next_token].startswith('-'):
                    options[tokens[token_index]] = tokens[next_token]
                    token_index += 2
                else:
                    options[tokens[token_index]] = None
                    token_index += 1
        # end while

        logging.info("Running report {} with options {}".format(report_name, options))
        report = self.appliance.run_sarg_command(report_name, options)

        if len(report['result']) == 0:
            logging.warning("No results for this report")
            return

        # Calculate lengths for nice display
        lengths = dict()
        total_length = len(report['result'][0].keys())
        format_length_list = list()

        for (k,v) in report['result'][0].items():
            lengths[k] = len(v)
            total_length += lengths[k]

            if len(k) > len(v):
                l = str(len(k))
                format_length_list.append("{:<" + l + "." + l + "}")
            else:
                l = str(lengths[k])
                format_length_list.append("{:<" + l + "." + l + "}")

        format_str = " ".join(format_length_list)
        logging.debug(format_str)
        logging.warning(format_str.format(*report['result'][0].keys()))

        for line in report['result']:
            logging.warning(format_str.format(*line.values()))
    # end

    def pretty_print_image_collection(self, imagecollection):
        """
        Pretty print an Actifio.ActImageCollection instance

        :param imagecollection: ActImageColletion to be pretty printed
        """
        logging.warning("\n{:<32} {:<25} {:<14} {:<10} {:<15} {:<16}".format(
            "image", "backupdate", "componenttype", "jobclass", "mountedhost", "host IP address"))

        for image in imagecollection:
            if image.mountedhost != "0":
                hosts = self.appliance.get_hosts(id=image.mountedhost)

                if len(hosts) == 0:
                    host = ""
                else:
                    host = hosts[0]

            logging.warning("{:<32} {:<25} {:<14} {:<10} {:<15} {:<16}".format(
                image.backupname, image.backupdate,
                image.componenttype, image.jobclass,
                "not mounted" if image.mountedhost == "0" else host.printable.split()[0],
                "not mounted" if image.mountedhost == "0" else host.objectdata['ipaddress']))

        for image in imagecollection:
            logging.debug(pformat(image.objectdata))
    # end

    def get_images_mounted_for_db(self, db):
        """
        Given a database `db`, return ImageCollection of images mounted for it on this
        appliance. None if no images found.

        :param db: database name
        :return: Actifio.ImageCollection of images mounted for the DB, or None
        """
        mnt = self.appliance.run_sarg_command('reportmountedimages', {})

        for item in mnt['result']:
            logging.info("get_images_mounted: MountAppName is {}".format(item['MountedAppName']))

            if item['MountedAppName'] == db:
                imagecollection = self.appliance.get_images(backupname=item['MountImageName'])
                logging.warning("{} image(s) mounted for database {}".format(len(imagecollection), db))
                self.pretty_print_image_collection(imagecollection)
                return imagecollection

        logging.warning("No mounted images found for db {}".format(db))
        return None
    # end

    def get_minimal_restore_time(self):
        """
        TODO: We need to find out the MINIMAL recoverytime for an image
        :return:
        """
        pass
    # end

    def check_job_running(self, job):
        """
        Refresh the job and return True if it is in the 'running' status

        :param job: ActJob instance
        :return: True if the job is running
        """
        job.refresh()

        if job.objectdata['status'] == 'running':
            return True
        return False
    # end

    def check_job_and_wait(self, job, detach=False):
        """
        Keep checking if job is in running status, sleeping for 60 seconds at each
        iteration. Prints the elapsed time when the job is completed, and if it was
        successful or failed.

        :param job: ActJob instance
        :param detach: if True, return immediately after confirming job is running
        :return: True if job completed successfully. False otherwise.
        """
        if not job:
            return False

        logging.debug('Job info: {}'.format(pformat(job.objectdata)))

        if detach:
            job.refresh()
            print('Detaching job. Last status: {}'.format(job.objectdata['status']))
            print("To query this job's status, use: -H {} -j {}".format(
                self.host, job.printable))
            return True

        start = time.time()

        if self.check_job_running(job):
            print("Job running (sleeping 60 seconds for each .): ", end='', flush=True)

            while self.check_job_running(job):
                time.sleep(60)
                print(".", end='', flush=True)
        # end if

        end = time.time()

        print('\n{} completed '.format(job), end='', flush=True)

        if job.objectdata['status'] == 'succeeded':
            print('successfully in {:.2f} seconds'.format(end - start), flush=True)
            return True
        else:
            print('WITH FAILURE in {:.2f} seconds'.format(end - start), flush=True)
            return False
    # end

    def unmount_image(self, mounted_image, delete=True, nowait=True):
        """
        Unmount and delete oracle image (if delete=True, default) from appliance.
        Requests immediate operation if nowait=True (default). If requesting an
        operation in a target_host with 'prd' in its name, it does NOT proceed.

        :param image: ActifioImage instance to be unmounted
        :param delete: if True (default), delete image as well
        :param nowait: if True (default), request immediate operation
        :return: Actifio Job instance, or False
        """
        # TODO: Check if image is actually mounted before proceeding
        # Let's make sure we really want this to run in production

        if 'prd' in mounted_image.objectdata['mountedhost']:
            logging.warning('Image is mounted on production host. Quitting.')
            sys.exit(-1)

        logging.warning("Unounting{} image {}".format(
            " and deleting" if delete else "", mounted_image.backupname))
        job = self.appliance.unmount_image(image=mounted_image, delete=delete, nowait=nowait)

        if job:
            logging.warning("Umount {}image: {}".format("and delete " if delete else "", job))
            return job
        else:
            logging.warning("Umount {}image job did not proceed".format("and delete " if delete else ""))
            return False
    # end

    def get_actual_image_name(self, image=Actifio.ActImage):
        """
        DEPRECATED

        Unlike the REST API, apparently the Actifio Python API returns image names
        prefixed with the host/identifier of the appliance where they were taken.
        But the API itself, in other operations, requires the actual image name,
        without the prefix. This method returns the actual image name.

        :param image: Actifio.ActImage instance
        :return: Actual name for the image, without prefix
        """
        if image.backupname.count('_') > 1:
            return image.backupname.split('_', 1)[1]
        else:
            return image.backupname
    # end

    def mount_image(self, db):
        """
        Requests a mountimage operation from the actifio appliance for `db`.
        Looks for the source application for db, then finds the latest image for that
        source application, and requests the mountimage for the configured target_host.

        :param db: Name of database to look for
        :return: ActJob instance with mount image job
        """
        logging.warning("Mounting image for db {}".format(db))

        if db not in self.config['db'].keys():
            logging.error("Missing config information for db. Need command-line options or config file. Exiting.")
            sys.exit(-1)

        apps = self.appliance.get_applications(appname=self.config['db'][db]['source_db'])

        if apps is Actifio.ActAppCollection and (len(apps) == 0 or len(apps) > 1):
            logging.error("No source application found, or more than one app named '{}' in appliance {}".format(
                self.config['db'][db]['source_db'], self.host))
            logging.error(pformat(apps))
            sys.exit(-1)

        app = apps[0]
        logging.debug("Application data is:")
        logging.debug(pformat(app.objectdata))

        # Need to filter on componenttype=0, otherwise (=1) it's an invalid log backup
        if app.objectdata['originalappid'] == "0":
            application_id = app.objectdata['id']
        else:
            application_id = app.objectdata['originalappid']

        images = self.appliance.get_images(appid=application_id,
                                           jobclass="Snapshot", componenttype="0")

        # If we find no images, see if they are a StreamSnap
        if len(images) == 0:
            images = self.appliance.get_images(appid=application_id,
                                               jobclass="StreamSnap", componenttype="0")

        logging.warning("Source application for image is: {}.".format(app))
        logging.warning("{} available images".format(len(images)))

        if len(images) == 0:
            logging.error("No images found for db {}".format(db))
            return False

        self.pretty_print_image_collection(images)
        image = self.find_latest_image(images)
        logging.warning("Latest image is: {} ({})".format(image, image.objectdata['backupdate']))
        logging.info("Image data: \n{}".format(pformat(image.objectdata)))

        try:
            # Check if target_host is a valid ip address, and if so, query for it
            ipaddress.ip_address(self.config['db'][db]['target_host'])
            hosts = self.appliance.get_hosts(ipaddress=self.config['db'][db]['target_host'])
        except ValueError:
            # target_host is a hostname
            hosts = self.appliance.get_hosts(hostname=self.config['db'][db]['target_host'])

        if len(hosts) != 1:
            logging.error("Zero or more hosts found for target host {} (db {}). Check config file.".format(self.config['db'][db]['target_host'], db))

            for host in hosts:
                logging.error(pformat(host))
            sys.exit(-1)
        else:
            target_host = hosts[0]
            logging.warning("Target host: {}. id={}".format(target_host, target_host.id))

        provisioning_opts = '''
          <provisioning-options>
            <databasesid>{0}</databasesid>
            <username>{1}</username>
            <orahome>{2}/{3}/dbhome_1</orahome>
            <tnsadmindir>{2}/{3}/dbhome_1/network/admin</tnsadmindir>
            <totalmemory>{4}</totalmemory>
            <rrecovery>true</rrecovery>
            <standalone>true</standalone>
            <sgapct>{5}</sgapct>
            <open_cursors>2000</open_cursors>
            <nonid>false</nonid>
            <noarchivemode>false</noarchivemode>
            <notnsupdate>false</notnsupdate>
            <CLEAR_OS_AUTHENT_PREFIX>false</CLEAR_OS_AUTHENT_PREFIX>
          </provisioning-options>'''.format(
              db, self.config['db'][db]['username'],
              self.config['db'][db]['orahome_prefix'],
              self.config['db'][db]['ora_version'],
              self.config['db'][db]['total_memory'],
              self.config['db'][db]['sga_pct']
          )

        # We should be able to use Actifio's simple_mount method, which would take
        # in arguments and process and check them, but the method has bugs, severe
        # ones, so we prep the arguments and call a task directly. Keeping the
        # simple_mount code here for reference. Maybe we can fix it later.

        #(job, retimage) = self.appliance.simple_mount(source_application=app, label=db,
        #                                              restoretime=self.to_datetime(image.objectdata['backupdate']),
        #                                              target_host=target_host,
        #                                              mount_image=image,
        #                                              mount_mode="physical", appaware=True,
        #                                              mountpointperimage='/act/mnt/{}'.format(db),
        #                                              recoverytime=self.get_quoted_recovery_time(self.config['db'][db]['recovery_time']),
        #                                              provisioningoptions=''.join(provisioning_opts.split()))
                                                      # post_script=post_script,
                                                      # restoreoption=restore_option)

        restore_option = 'mountpointperimage=/act/mnt/{}'.format(db)

        # if a recovery_time is present in the config file, use that to request a Point-In-Time
        # recovery is done from the image that Actifio retrieves, to up to that time.
        # otherwise, use the current time

        if self.config['db'][db]['recovery_time']:
            config_recovery_time = datetime.strptime(self.config['db'][db]['recovery_time'], '%H:%M:%S')
            recovery_time = datetime.combine(self.current_ts.date(), config_recovery_time.time())
        else:
            recovery_time = self.current_ts.strftime('%Y-%m-%d %H:%M:%S')

        # If we want to have actifio run post-mount scripts again
        #post_script = ('actifio-post-kyle.sh:args=ylprd,ylvostg')

        mountimage_args = {
            'image': image.backupname,
            'label': db,
            'host': target_host.id,
            'nowait': 'true',
            'rdmmode': 'physical',
            'appaware': 'true',
            'recoverytime': recovery_time,
            'restoreoption': restore_option
        }
        # TODO I believe target_host.id is null when the VM in Actifio doesn't have an IP address, we need to provide useful error messages
        logging.warning("About to submit mount image request with the following parameters:")
        logging.warning(pformat(mountimage_args))
        logging.warning(provisioning_opts)

        # Add provisioning options to the restoreoption element
        mountimage_args['restoreoption'] += ',provisioningoptions={}'.format(
            ''.join(provisioning_opts.split()))

        # Request the mount operation
        mountimage_out = self.appliance.run_uds_command('task', 'mountimage', mountimage_args)
        output = mountimage_out['result'].split(" ")
        job = self.appliance.get_jobs(jobname=output[0])[0]
        returned_image = self.appliance.get_images(backupname=output[3])[0]
        logging.warning("Mount image started: {}. Returned image: {}.".format(
            job, returned_image))
        return job
    # end
# end class


def get_parser(args=sys.argv[1:]):
    """
    Get parser for arguments from CLI. Performs some option checking.
    """
    parser = argparse.ArgumentParser(description='Work with Actifio appliances')

    parser.add_argument('-H', '--host',
                        action='store', required=False,
                        help='Actifio appliance hostname')

    parser.add_argument('-U', '--username',
                        action='store', required=False,
                        help='Username to connect to actifio')

    parser.add_argument('-p', '--password',
                        action='store', required=False,
                        help='Password to connect to actifio (if not passed, will ask interactively)')

    #parser.add_argument('-n', '--dry-run',
    #                    action='store_true', required=False, default=False,
    #                    help='Do not actually run commands')

    parser.add_argument('-c', '--config',
                        action='store', required=False, default="actifio.yml",
                        help='Load yaml config file with database details')

    parser.add_argument('-v', '--verbose',
                        action='count', default=0,
                        help="Log verbosely. Use more -v's to increase verbosity")

    parser.add_argument('-d', '--detach',
                        action='store_true', required=False, default=False,
                        help='Detach immediately after starting a job, instead of waiting for its completion')

    parser.add_argument('-j', '--job-status',
                    action='store', nargs=1, metavar='JOBNAME',
                    help='Outputs status for job')

    parser.add_argument('-J', '--list-running-jobs',
                    action='store_true',
                    help='Outputs list of running on-demand jobs (scheduled jobs are not shown, for that use -o reportrunningjobs)')

    parser.add_argument('-l', '--list-images',
                    action='store', nargs=1, metavar='DB',
                    help='List images for database')

    parser.add_argument('-R', '--list-reports',
                    action='store_true',
                    help='Outputs list of available reports')

    parser.add_argument('-r', '--report',
                    action='store', metavar='REPORT',
                    help='Runs report')

    parser.add_argument('-o', '--report-options',
                        action='store', metavar='REPORT_OPTIONS', default="",
                        help='Options for report to be run by -r (example: -o="-a ylprd"')

    parser.add_argument('-u', '--unmount-delete',
                    action='store', required=False, metavar='DBNAME',
                    help='Unmount and delete oracle image for database')

    parser.add_argument('-m', '--mount-image',
                    action='store', required=False, metavar='DBNAME',
                    help='Mount oracle image')

    parser.add_argument('-M', '--mount-after-unmount-delete',
                    action='store', required=False, metavar='DBNAME',
                    help='Mount oracle image after unmount and delete (implies -u)')

    parser.add_argument('--source-db',
                    action='store', required=False, metavar='DBNAME',
                    help='Source database of the image (e.g. ylprd, ylvoprd, yls1)')

    parser.add_argument('--target-db',
                    action='store', required=False, metavar='DBNAME',
                    help='Target database to be mounted')

    parser.add_argument('--target-host',
                    action='store', required=False, metavar='TARGET_HOST',
                    help='Target host where mount image will be mounted')

    parser.add_argument('--recovery-time',
                    action='store', required=False, metavar='TIME',
                    help='PITR recovery option (format should be 03:00:00)')

    parser.add_argument('--ora-version',
                    action='store', required=False, nargs=1, metavar='VER',
                    help='Dotted Oracle version (i.e. 11.2.0, 12.1.0, 12.2.0)')

    parser.add_argument('--orahome-prefix',
                    action='store', required=False, nargs=1, metavar='PATH',
                    default='',
                    help='ORA_HOME path prefix where image will be mounted (ora-version will be appended)')

    parser.add_argument('--total-memory',
                    action='store', required=False, nargs=1, metavar='MB',
                    help='Total memory to be made available to new mounted image')

    processed_args = parser.parse_args(args)

    # at least one of these options has to be passed in the command-line
    options = {'job_status', 'list_running_jobs', 'list_images', 'list_reports', 'report',
               'unmount_delete', 'mount_image', 'mount_after_unmount_delete'}

    processed_args_vars = vars(processed_args)
    option_values = [processed_args_vars[opt] for opt in options]

    if not any(option_values):
        logging.error('Need at least one action to execute. Exiting.')
        sys.exit(-1)

    if 'unmount_delete' in processed_args or 'mount_after_unmount_delete' in processed_args or \
            'mount_image' in processed_args:
        if (processed_args.unmount_delete and processed_args.mount_after_unmount_delete) or \
                (processed_args.mount_image and processed_args.mount_after_unmount_delete):
            logging.error("Conflicting options: -u and -M or -m and -M")
            sys.exit(-1)
    # end

    return processed_args
# end


def read_db_config(fname: str):
    """
    Reads a YAML db configuration file
    :param fname: path to the config file
    :return:
    """
    with open(fname, 'r') as fin:
        dbs = yaml.safe_load(fin)
# end


def setup_logging(verbose_level, no_logging=False):
    """
    Setup logging module according to verbose_level. If no_logging is True,
    no logging will be done.

    :param verbose_level: level of logging verbosity
    :param no_logging: if True, no logging will be done
    """
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    level = levels[min(len(levels) - 1, verbose_level)]

    if no_logging:
        logging.basicConfig(level=level, format="%(message)s", handlers=[logging.NullHandler])
    else:
        logging.basicConfig(level=level, format="%(message)s", stream=sys.stdout)
# end

def load_config(args):
    defaults = {
        'actifio': {
            'verbose': True,
            'dry_run': True
        },
        'db': {}
    }

    if args.config and not os.path.isfile(os.path.abspath(args.config)):
        logging.error('config file is not a regular file: {}'.format(os.path.abspath(args.config)))
        sys.exit(-1)

    try:
        with open(args.config, "r") as fin:
            required_keys = []
            config = yaml.safe_load(fin)
            config = TruemarkActifio.recursive_merge(defaults, config)
            logging.warning("Loaded config file {}".format(args.config))

            # Let's get the DB name from one of the image operations, if passed
            image_operations = ['mount_image', 'unmount_delete', 'mount_after_unmount_delete']
            image_db = None

            for operation in image_operations:
                image_db = getattr(args, operation)

                # If we get a valid option, just exit out of the for loop, as only
                # one of these options can be passed

                if image_db:
                    break

            if image_db:
                # Merge in image options passed on the command-line

                image_options = ['source_db', 'target_db', 'target_host', 'recovery_time',
                                 'ora_version', 'orahome_prefix', 'total_memory']
                for option in image_options:
                    option_value = getattr(args, option)

                    if option_value:
                        if option in config['db'][image_db].keys():
                            old_value = config['db'][image_db][option]
                            logging.warning('DB: {}. Replacing config option {}: old={} new={}'.format(
                                image_db, option, old_value, option_value))
                            config['db'][image_db][option] = option_value
                        else:
                            logging.warning('DB: {}. Setting new config option {} = {}'.format(
                                image_db, option, option_value))
                    # end if
                # end for

            logging.debug(pformat(config))
            return config
    except FileNotFoundError as e:
        logging.error('config file not found: {}'.format(args.config))
        sys.exit(-1)
    except KeyError as e:
        logging.error("Invalid config file. Missing 'actifio' section")
        sys.exit(-1)
# end


def get_actifio_host_from_args_or_config(db, args, config_dict):
    """
    In image-related operations (mount, unmount, etc) we need to use the actifio host
    of the target database, not the main host in the actifio section of the config file.
    This function returns the host from the config file, or the one from the command-line
    argument, if passed.

    :param db: database name
    :param args: ArgParser namespace instance
    :param config_dict: dictionary with default config values
    :return: a configuration dictionary with values merged in
    """
    if 'db' in config_dict.keys() and db in config_dict['db'].keys():
        try:
            return config_dict['db'][db]['host']
        except KeyError:
            logging.error("No host in config file for db {}".format(db))

            if 'host' in config_dict['actifio'].keys():
                logging.warning("Using default host from config: {}".format(config_dict['actifio']['host']))
                return config_dict['actifio']['host']
    elif args.host:
        logging.warning("Using {} as actifio host".format(args.host))

    logging.error("Need an actifio host to connect to, and none was passed in the command-line")
    logging.error("or is present in the config file (did you use a valid DB name?)")
    sys.exit(-1)
# end


def main():
    args = get_parser()
    config_dict = {'actifio': {'verbose': 0, 'dry_run': False}}  # defaults
    setup_logging(max(args.verbose, config_dict['actifio']['verbose']))

    if args.config:
        config_dict = load_config(args)

    setup_logging(max(args.verbose, config_dict['actifio']['verbose']))
    logging.debug("Arguments: {}".format(args))

    if not args.username:
        if 'username' not in config_dict['actifio'].keys():
            logging.error("Username is required either in the command-line or config file")
            sys.exit(-1)
        else:
            args.username = config_dict['actifio']['username']

    if not args.password:
        if 'password' not in config_dict['actifio'].keys() or \
                config_dict['actifio']['password'] is None:
            args.password = getpass(prompt="Password for actifio appliance: ")
        else:
            args.password = config_dict['actifio']['password']

    if args.mount_image:
        db = args.mount_image
        args.host = get_actifio_host_from_args_or_config(db, args, config_dict)
        tma = TruemarkActifio(args.host, args.username, args.password, verbose=args.verbose)
        tma.load_config(config_dict)
        job = tma.mount_image(db)

        if tma.check_job_and_wait(job, args.detach):
            sys.exit(0)
        else:
            sys.exit(-1)
    # end if

    if args.unmount_delete or args.mount_after_unmount_delete:
        db = args.unmount_delete or args.mount_after_unmount_delete
        args.host = get_actifio_host_from_args_or_config(db, args, config_dict)
        tma = TruemarkActifio(args.host, args.username, args.password, verbose=args.verbose)
        tma.load_config(config_dict)
        mounted_images = tma.get_images_mounted_for_db(db)
        exit_code = 0

        if mounted_images:
            # Don't really think this is necessary
            if len(mounted_images) > 1:
                latest_image = tma.find_latest_image(mounted_images)
            else:
                latest_image = mounted_images[0]

            job = tma.unmount_image(latest_image, delete=True, nowait=True)

            if job:
                if args.mount_after_unmount_delete:
                    # Because we're going to run another operation, we can't detach
                    tma.check_job_and_wait(job)
                    exit_code = 0
            else:
                logging.error("Failed to start unmount job, or unmount failed.")
                sys.exit(-1)
        else:
            logging.warning("No mounted images for db {}. ".format(db))

        if args.mount_after_unmount_delete:
            job = tma.mount_image(db)

            if tma.check_job_and_wait(job, args.detach):
                sys.exit(0)
            else:
                sys.exit(-1)
        else:
            sys.exit(exit_code)
    # end if

    if args.list_images:
        logging.warning("Listing (mounted) images for database {} (use -v for more info)".format(args.list_images[0]))
        args.host = get_actifio_host_from_args_or_config(args.list_images[0], args, config_dict)
        tma = TruemarkActifio(args.host, args.username, args.password, verbose=args.verbose)
        images = tma.get_images_mounted_for_db(args.list_images[0])

        if not images:
            sys.exit(0)

        latest = tma.find_latest_image(images)
        logging.warning("Latest image is: {}".format(latest))
        logging.info("Image data: \n{}".format(pformat(latest.objectdata)))
        opts = [prov_opt.printable for prov_opt in latest.provisioningoptions()]
        logging.info("Image provisioning options: {}".format(opts))
    # end if

    # The following are informational commands, where we need the actifio host to
    # connect to, either passed in the config file in the actifio section, or
    # through the -H command-line argument (the latter takes precedence, if passed)

    try:
        host = args.host or config_dict['actifio']['host']
    except KeyError:
        logging.error("Need a host to connect to (-H or config file)")
        sys.exit(-1)

    tma = TruemarkActifio(host, args.username, args.password, verbose=args.verbose)

    if args.list_reports:
        tma.run_report('reportlist')
        sys.exit(0)
    # end

    if args.report:
        tma.run_report(args.report, args.report_options)
        sys.exit(0)
    # end

    if args.list_running_jobs:
      tma.run_report('reportrunningjobs', "-o")
      sys.exit(0)
    # end if

    if args.job_status:
        jobs = tma.appliance.get_jobs(jobname=args.job_status[0])

        if len(jobs) == 0:
            logging.error("No job {} found".format(args.job_status[0]))
            sys.exit(-1)
        else:
            logging.warning(pformat(jobs[0].objectdata))
            sys.exit(0)
    # end if
# end


if __name__ == '__main__':
    main()

