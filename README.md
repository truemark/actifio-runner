# tmactifio.py - work with Actifio appliances

tmactifio.py performs operations on Actifio appliances using the REST API. 
Currently (January 2020) it can only perform a few operations, such as listing images,
reports, job status, and perform some image operations, such unmounting/deleting 
mounted images, and mounting images to a target database.

It can be run directly with a python virtual environment (`source setup.sh`) or 
through a docker image. Currently it is setup to build and push to 
hub.docker.org as `truemark/actifio-runner`

## Configuration file

You can pass all arguments on the command-line, or you can use a config file with the 
`-c`flag. An example `config.yml`is provided on this repository.

If an operation is requested to be performed in a database present in the config file,
then the values from that host will be used, such as `host`, `target_host`, etc. If no
config file is passed, the options need to be passed as arguments on the command-line.

`tmactifio.py` does very minimal checking. In image operations, it checks the 
`target_host` field of the configuration file for a database, and if it contains 
`prd` anywhere, it will NOT proceed. But that will be inneffective if the 
`target_host` is an ip address, for example, and it happens to be a production one.

So BE CAREFUL when performing image operations.

### Password security

If you're going to run this on a shared machine, DO NOT pass your password on the
command-line, because it will be visible to other processes, and that is a security
risk.

Instead, on a shared machine, do not pass the `-p (--password)` argument, and the
program will request your password interactively.

## Usage

### Detached and non-detached modes

By default, when performing operations, `tmactifio.py` will wait for the requested
jobs to be completed. However, the user can request `-d (--detached)` mode on the
command-line. That will cause the job to be started, with information on how to query
for that job, and then the program will exit.

An example of a dettached job would be as such:

```
Detaching job. Last status: running
To query this job's status use: -H myactifiohost.com -j Job_22427764
```

With this info, the user could run something like:

`docker run --rm -it truemark/actifio-runner -U mysuser -H myactifiohost.com -j Job_22427764`

Alternatively, you could just use the Actifio appliance hostname and ask for the
report of all of the user-initiated running jobs.

`python3 tmactifio.py -H my-actifio.host -U myuser -J`

### Sample uses

The examples below can be used both invoking through Python directly, or 
the docker image.

Mount new backup of source application for database `mydb` AFTER unmount/deleting it
(this is the equivalent of running a `-u` followed by a `-m` operation, as shown below)

`python3 tmactifio.py -U myuser -p mypass -M mydb`

Unmount and Delete latest image for database `mydb`

`python3 tmactifio.py -U myuser -p mypass -u mydb`

Mount new backup of source application for database `mydb`

`python3 tmactifio.py -U myuser -p mypass -m mydb`

List mounted images for database `mydb`

`python3 tmactifio.py -H my-actifio.host -U myuser -p mypass -l ylenv6`

Show status of `Job_123456` in `my-actifio.host`

`python3 tmactifio.py -H my-actifio.host -U myuser -p mypass -j Job_123456`

Show list of running jobs on appliance `my-actifio.host`

`python3 tmactifio.py -H my-actifio.host -U myuser -p mypass -J`

Show reports available in `my-actifio.host`

`python3 tmactifio.py -H my-actifio.host -U myuser -p mypass -R`

Run `reportimages` report with option to filter by application `myapp`. 
Options for the different reports can be found on the SARG User Guide.

`python3 tmactifio.py -H my-actifio.host -U myuser -p mypass -r reportimages -o="-a myapp"`


