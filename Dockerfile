FROM python:3
COPY tmactifio.py /
COPY requirements.txt /
RUN pip install --no-cache --upgrade -r requirements.txt
ENTRYPOINT [ "python3", "./tmactifio.py" ]
