#!/usr/bin/env bash

install_pips() {
    python3 -m pip install --upgrade pip
    pip install --no-cache --upgrade -r requirements.txt
}

create_venv() {
    if [ ! -d "python-venv" ]; then
   	    python3 -m venv python-venv
    fi
    source python-venv/bin/activate
    install_pips
}

DIR="$(dirname "${0}")"
cd "${DIR}/.." || exit 1

echo "Configuring python-venv virtual environment"
create_venv
